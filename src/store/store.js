import { createStore, compose } from 'redux';
import { rootReducer } from './reducers/rootReducer';


let composedEnhancers = compose;

if (__DEV__) {
    composedEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

export const store = (
    createStore(
        rootReducer,
        composedEnhancers(),
    ));
