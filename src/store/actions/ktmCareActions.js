import { ktmCareConstants } from '../../common/enums/ktmCareConstants';

export function setCustomerInfo(value){
    return {
        type : ktmCareConstants.UI_SET_CUSTOMER_INFO,
        payload : value
    };
}


