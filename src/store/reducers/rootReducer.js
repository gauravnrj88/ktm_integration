import { combineReducers } from 'redux';
import { ktmCareReducer } from './ktmCareReducer';

export const rootReducer = combineReducers({
    ktmCare: ktmCareReducer 
});
