import { ktmCareConstants } from '../../common/enums/ktmCareConstants';
import { createReducer } from './createReducer';

const initialState = {
    // sample customerInfo object
    customerInfo: {
        "AddressLine1": "Ganesh villa",
        "B2CUserName": "ktmdev@mailinator.com",
        "Email": "ktmdev@mailinator.com",
        "FirstName": "KTM",
        "Gender": "Male",
        "Hobbies": "RockClimbing",
        "LastName": "Dev",
        "MobilePhone": "9890142660"
    },
};

export const ktmCareReducer = createReducer(initialState, {
    [ktmCareConstants.UI_SET_CUSTOMER_INFO]: setCustomerInfo,
});

function setCustomerInfo(state, action) {
    return {
        ...state,
        customerInfo: action.payload
    }
}








