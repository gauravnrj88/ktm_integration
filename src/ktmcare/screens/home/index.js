import React from 'react';
import {View, TouchableOpacity, Text, StatusBar} from 'react-native';

export default function Home(props){

    function _onKTMProClicked(){
        props.navigation.navigate("KTMProRoute");
    }

    
    return(
        <View style={{flex: 1, backgroundColor: '#fa6603', justifyContent: 'center', alignItems: 'center'}}>
            <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
            <Text style={{fontSize: 20, color: 'white', }}>
                KTM Care home page
            </Text>

            <TouchableOpacity 
                onPress={_onKTMProClicked}
                style={{borderColor: 'white', borderWidth: 1, borderRadius:5,
                paddingHorizontal:20, paddingVertical: 10, marginTop: 30}}>
                <Text style={{color: 'white', fontSize: 16}}>
                    KTM Pro XP
                </Text>
            </TouchableOpacity>
        </View>
    )
}