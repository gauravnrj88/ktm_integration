/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @flow
 * @format
 */

['danger', '', 'secondary', 'link-color', 'pink', 'brown', 'moment-txt', 'find-device', 'traveller-para'];

'use strict';

export default {
	white: '#ffffff',
	whiteShade: '#D3D3D3',
	grey: '#9b9b9b',
	greylight: '#ECECEC',
	greyDark : '#909090',
	greyDim: '#c6c6c6',
	light: '#f4f4f4',
	lightblack: '#333333',
	darkblack: '#4D4D4D',
	blue: '#4a90e2',
	lightblue: '#4A90E2',
	darkblue: '#57A5DD',
	textblue:'#29ABE2',
	lightgrey: '#b3b3b3',
	orange: '#ec9505',
	orangelight: '#FDF5E8',
	green: '#459d72',
	greenTrans : 'rgba(47,190,95,0.5)',
	greenlight: '#33CD67',
	darkgreen:'#22B573',
	black: '#000000',
	red: '#e21836',
	darkred: '#b82626',
	cyan: '#06e6e9',
	cyanlight: '#00E3E5',
	cyandrak:'#06E6E9',
	yellow: '#FFBC01',
	darklight: '#b3b3b3',
	bglight: '#f0f2f7',
	btncolor: '#00A99D',
	maptextcolor: '#4A4A4A',
	mapsmalltextcolor: '#9B9B9B',
	triptextcolor: '#B3B3B3',
	tabTextcolor:'#999999',
	warning:'#F7931E',
	tagbg:'#F2F2F2',
	bordercolor:'#CCCCCC',
	pointtext:'#808080',
	datebox:'#E6E6E6',
	manualtaggrey:'#B4B4B4',
	introtext:'#33C966',
	introinfotext:'#5BA7DC',
	modaltagcolor:'#E4E6EC',
    modaltagtextcolor:'#4C4F55',
	chatBox:'#EAEAEA',
	// stop location colors
	danger:     '#f53d3d',

	secondary:  '#32db64',
	linkcolor: '#24a1de',
	pink:     '#ff9797',
	brown:      '#C69C6D',
	momenttxt: '#8398B9',
	finddevice: '#fd6376',
	travellerpara:'rgb(148,147,147)',
	darkblackactive: '#666666',

	headerRight: '#373744',
	alertbg: '#41A783',
	linecolor: '#373744',
	alertbgred: '#CF0707',
	modalbg: '#161616',
	modalborder: '#979797',
	modalbordergreen: '#0CD18B',

	searchbox: '#FAFAFA',
	searchborder: '#E2E2E2',
	listbdcolor: '#D8D8D8',
	tripstext: '#636363',
	tripstext2: '#A2A2A2',
	cardcolor: '#282828',
	cardcolorShade : "#494949",
	imgbg: '#A2A2A2',
	newtag: '#5788EB',

	moretext : '#BABABA',

	
	livecolor: '#332519',
  livename: '#848484',
  moretext: '#BABABA',
  bbcolor: '#41A783',
  newtextcolor: '#ECF5FF',
  livemapcolor:'#332519',
  livemapsprotext:'#606066',
  activelivelist:'#E7E7E7',
  activetextcolor:'#393939',
  groupbg:'#191919',

  greyBlue: '#5B607E',
	blackDarker:'#191919', 

	greenishGrey: "#9CA998",
	greenish: "#98FB98" ,


	moretext : '#BABABA',

	
	livecolor: '#332519',
  livename: '#848484',
  moretext: '#BABABA',
  bbcolor: '#41A783',
  newtextcolor: '#ECF5FF',
  livemapcolor:'#332519',
  livemapsprotext:'#606066',
  activelivelist:'#E7E7E7',
  activetextcolor:'#393939',
  groupbg:'#191919',

  greyBlue: '#5B607E',
	blackDarker:'#191919', 

	greenishGrey: "#9CA998",
	greenish: "#98FB98" ,

  ktm_color:'#fa6603',
  ktm_color_light:'#db5e04'
};
