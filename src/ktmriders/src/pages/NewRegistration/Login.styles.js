import {StyleSheet, Dimensions} from 'react-native';
import Colors from '../../assets/js/Colors';
import ResponsiveFontUtil from '../../utils/ResponsiveFontUtil';

const {width: WIDTH, height: HEIGHT} = Dimensions.get('window');

export default StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: WIDTH,
    resizeMode: 'cover',
  },
  newlogin: {
    width: WIDTH,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Alreadytext: {
    fontSize:ResponsiveFontUtil(12),
    fontFamily: 'Avenir-Heavy',
    color: Colors.lightblack,
  },
  loginTextInput: {
    width: WIDTH - 100,
    justifyContent: 'center',
    // height: 40,
    borderBottomWidth: 2,
    borderBottomColor: Colors.lightblack,
    color: Colors.lightblack,
    textAlign: 'left',
    fontSize: ResponsiveFontUtil(13),
    fontFamily: 'Avenir-Medium',
  },
  signinbtn: {
    width: WIDTH - 110,
    justifyContent: 'center',
    height: 40,
    flexDirection:'row',
    // borderWidth: 2,
    // borderColor: Colors.introtext,
    borderRadius: 20,
    alignItems: 'center',
    marginVertical:10
  },
  signinbtntext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: ResponsiveFontUtil(14),
    color: Colors.lightblack,
  },
  signinbtnmargin: {
    marginTop: '40%',
  },
  signinbtnmargintop: {
    marginBottom: 15,
  },
  signupbtn: {
    width: WIDTH - 110,
    justifyContent: 'center',
    height: 40,
    flexDirection:'row',
    // borderWidth: 2,
    // borderColor: Colors.introinfotext,
    borderRadius: 20,
    alignItems: 'center',
    // marginTop:100
  },
 
  dontHaveText:{
    fontFamily: 'Avenir-Medium',
    fontSize: ResponsiveFontUtil(12),
    color: Colors.darkblack,
    marginTop:60
  },

  signupbtntext: {
    fontFamily: 'Avenir-Medium',
    fontSize: ResponsiveFontUtil(13),
    color: Colors.lightblack,
    textDecorationLine: 'underline',
  },
  newsignup: {
    width: WIDTH,
    // height: HEIGHT,
    // paddingTop: 20,
    alignItems: 'center',
  },
  newprofile: {
    width: 70,
    height: 70,
    backgroundColor: '#000',
    borderRadius: 60,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  newprofileimg: {
    width: 70,
    height: 70,
    borderRadius: 60,
    overflow: 'hidden',
  },
  signupTextInput: {
    width: WIDTH - 60,
    justifyContent: 'center',
    // height: 40,
    borderBottomWidth: 2,
    borderBottomColor:  Colors.lightblack,
    color:  Colors.lightblack,
    textAlign: 'left',
    fontSize: ResponsiveFontUtil(16),
    fontFamily: 'Avenir-Light',
  },
  signupPhoneTextInput: {
    width: WIDTH - 60,
    justifyContent: 'center',
    height: 40,
    borderBottomWidth: 2,
    borderBottomColor: '#D8D8D8A6',
    color:  Colors.lightblack,
    textAlign: 'left',
    fontSize: ResponsiveFontUtil(16),
    fontFamily: 'Avenir-Light',
  },
  signupchecktext:{
      fontFamily:'Avenir-Medium',
      color:Colors.lightblack,
      fontSize:ResponsiveFontUtil(11),
  },

  resendcontainer:{
    // width:WIDTH-25,
    alignSelf:'center',
    flexDirection:'row',
    marginVertical:0,
   
},
resendText:{
    textAlign:'center',
    fontSize:ResponsiveFontUtil(8),
    fontFamily:'Gotham-Book',
    color:Colors.grey,
    width:WIDTH-25,
    fontWeight:'600'
},
resendbtn:{

    // width:WIDTH-90,
    // paddingVertical:8,
    // backgroundColor:Colors.black,
    // alignSelf:'center',
    // borderRadius:40,
    // marginVertical:10,
    
   },

resendbtnText:{
      textAlignVertical:'center',
      textAlign:'center',
      color: '#57A5DD',
      fontSize:ResponsiveFontUtil(7),
      fontFamily:'Gotham-Book',
      fontWeight:'300',
      textTransform:'uppercase'
   },
   signupPhoneTextInput: {
    width: WIDTH - 60,
    justifyContent: 'center',
    height: 40,
    borderBottomWidth: 2,
    borderBottomColor: Colors.lightblack,
    color: Colors.white,
    textAlign: 'left',
    fontSize: ResponsiveFontUtil(14),
    fontFamily: 'Avenir-Light',
  },

});
