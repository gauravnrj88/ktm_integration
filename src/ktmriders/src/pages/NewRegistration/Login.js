import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StatusBar,
  Image,
  TextInput,} from 'react-native';
import styles from './Login.styles';
import PhoneInput from 'react-native-phone-input';
const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');
import Colors from '../../assets/js/Colors';
import ResponsiveFontUtil from '../../utils/ResponsiveFontUtil';
import { connect } from 'react-redux';
import { setCustomerInfo } from '../../../../store/actions/ktmCareActions';
// sample
class NewLogin extends Component {

  constructor(props){
    super(props)
    this.state={
      mobileNo: ""
    }
  }

  componentDidMount(){
    //getting customerInfo from redux store
    const { customerInfo } = this.props.ktmCare;
    console.log('CustomerInfo: ', customerInfo)

  }

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };


  render() {
    return (
      <View style={{flex: 1,backgroundColor:Colors.ktm_color}}>
        <StatusBar translucent backgroundColor={Colors.ktm_color_light} barStyle="dark-content" />
       
          <View style={styles.newlogin}>
            
            <View style={{ marginBottom: 0 }}>
              
              <Image source={require('../../assets/images/ktm-small.png')}  style={{width:140,height:50}}/>
                
            </View>
            <View style={{ marginBottom: 30 }}>
              <Text style={styles.Alreadytext}>RIDES APP</Text>
            </View>
            <View style={[styles.signupPhoneTextInput,{ marginBottom: 20 , width: WIDTH-100}]}>
              
                <TextInput
                  value={this.state.mobileNo}
                  placeholder="ENTER YOUR MOBILE NUMBER"
                  placeholderTextColor={Colors.black}
                  onChangeText={text => this.setState({mobileNo: text})}
                />
                
            </View>

            <TouchableOpacity
              style={[styles.signupbtn]}
              activeOpacity={0.6}>
              <Text style={styles.signinbtntext}>SIGN IN</Text>
            </TouchableOpacity>

            <View>
              <Text style={styles.dontHaveText}>
                DON'T HAVE RIDES APP ACCOUNT ?
              </Text>
            </View>
            <TouchableOpacity
              style={[styles.signupbtn]}
              activeOpacity={0.6}>
              <Text style={styles.signinbtntext}>SIGN UP</Text>
            </TouchableOpacity>
          </View>
        
      </View>
    );
  }
}

const mapStateToProp = store => ({
  ktmCare: store.ktmCare
});

const mapDispatchToProps = {
  setCustomerInfo
};

export default connect(mapStateToProp, mapDispatchToProps)(NewLogin);