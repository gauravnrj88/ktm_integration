/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

 import React, {Component} from 'react';
 import {MenuProvider} from 'react-native-popup-menu';
 import {createAppContainer, createSwitchNavigator} from 'react-navigation';
 import {createStackNavigator} from 'react-navigation-stack';
 
 import LoginScreen from './src/pages/NewRegistration/Login';
 
 const AuthStack = createStackNavigator(
   {
     Login: {screen: LoginScreen},
   },
   {
     initialRouteName: 'Login',
     navigationOptions: {header: null},
   },
 );
 
 const StartTripNavigation = createAppContainer(
   createSwitchNavigator(
     {
       AuthScreens: AuthStack,
     },
     {
       initialRouteName: 'AuthScreens',
     },
   ),
 );
 
 
 export default class App extends Component {
   render() {
     
     return (
       <MenuProvider>
          <StartTripNavigation/>
       </MenuProvider>
     );
   }
 
   
 }
 