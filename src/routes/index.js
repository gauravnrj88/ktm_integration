import React from "react";
import { StyleSheet, Platform } from "react-native";
import {createAppContainer,createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import HomePage from "../ktmcare/screens/home";
import KTMProRoute from '../ktmriders/KTM_Pro_App';



const mainPageStack = createStackNavigator({
    HomePage: {
        screen: HomePage,
        navigationOptions: ({ navigation }) => ({
            header: null,
            headerBackTitle: null
        })
    },
    KTMProRoute : {
        screen: KTMProRoute,
        navigationOptions: ({ navigation }) => ({
            header: null,
            headerBackTitle: null
        })
    }
},
{
    initialRouteName: 'HomePage',
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: '#000000',
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
        },
        headerLeftContainerStyle: {
            marginTop: Platform.OS === 'ios' ? 20 : 0
        },
        headerRightContainerStyle: {
            marginTop: Platform.OS === 'ios' ? 20 : 0
        },
        headerTitleContainerStyle: {
            marginTop: Platform.OS === 'ios' ? 20 : 0
        }
    }
});

const switchNavigator = createSwitchNavigator({
    MainPage: mainPageStack
});

export const AppContainer = createAppContainer(switchNavigator);

export default AppContainer;